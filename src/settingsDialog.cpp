#include "settingsDialog.h"
#include "ui_settingsDialog.h"

#include <QFileDialog>
#include <QSettings>

SettingsDialog::SettingsDialog(QWidget* parent)
    : QDialog(parent)
    , ui(new Ui::SettingsDialog)
{
    ui->setupUi(this);

    QSettings settings;

    ui->refreshRate->setRange(10, 999);
    ui->refreshRate->setValue(settings.value("refreshRateHz").toInt());

    ui->loggingPathLabel->setText(settings.value("loggingDirPath").toString());

    connect(ui->loggingPathBtn, &QPushButton::clicked, [this]() {
        QFileDialog dialog(nullptr, "Select directory to store logs");
        dialog.setFileMode(QFileDialog::Directory);
        dialog.setFilter(QDir::Dirs | QDir::Writable);

        auto result = dialog.exec();
        if (result == QDialog::Accepted)
            ui->loggingPathLabel->setText(dialog.directory().absolutePath());
    });

    connect(this, &QDialog::accepted, [this]() {
        QSettings settings;
        settings.setValue("refreshRateHz", ui->refreshRate->value());
        settings.setValue("loggingDirPath", ui->loggingPathLabel->text());
    });
}

SettingsDialog::~SettingsDialog()
{
    delete ui;
}

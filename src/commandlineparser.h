#ifndef COMMANDLINEPARSER_H
#define COMMANDLINEPARSER_H
#include <QCommandLineParser>

class CommandLineParser : public QCommandLineParser
{
public:
    CommandLineParser();
    bool    areInputParamsCorrect();
    int     getReturnCode();
private:
     QCommandLineOption baudRate;
     QCommandLineOption serialPort;
     QCommandLineOption skipCrc;
     int                returnCode;
};

#endif // COMMANDLINEPARSER_H

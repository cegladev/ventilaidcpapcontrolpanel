#include "fileLogger.h"

#include <QDebug>
#include <QFile>
#include <QTextStream>

FileLogger::FileLogger(QObject* parent, QString filePath, QString header)
    : QObject(parent)
    , file(new QFile(filePath, this))
{
    bool newFile = !file->exists();

    if (!file->open(QIODevice::Append | QIODevice::Text)) {
        auto errorMsg = QString("Opening %1 failed with: %2").arg(filePath).arg(file->errorString());
        qDebug() << errorMsg;

        // TODO: is it the best way to handle this?
        throw std::runtime_error(errorMsg.toStdString());
    }

    if (newFile)
        onNewMessage(header);
}

void FileLogger::onNewMessage(QString msg)
{
    QTextStream out(file);
    out.setCodec("UTF-8");
    out << msg;
}

#ifndef UTILS_H
#define UTILS_H

#include <QString>

class Utils {
public:
    enum class Target {
        Desktop,
        Raspberry
    };

    static void initSettingsWithDefult();
    static bool isPathWritable(QString path);
};

#endif // UTILS_H

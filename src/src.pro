QT       += core gui serialport charts

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = VentilAid
TEMPLATE = app

include(GitVersion.pri)

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS
# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0
CONFIG += c++17

SOURCES += \
        commandlineparser.cpp \
        debugIntegerDialog.cpp \
        aboutDialog.cpp \
        chartWidget.cpp \
        debugDialog.cpp \
        fileLogger.cpp \
        main.cpp \
        mainWindow.cpp \
        message/additionalSettingsMessage.cpp \
        message/alertMessage.cpp \
        message/baseMessage.cpp \
        message/debugMessage.cpp \
        message/errorMessage.cpp \
        messageHandler.cpp \
        portSelectionDialog.cpp \
        runtimeconfig.cpp \
        serialOverseer.cpp \
        sendMessageDialog.cpp \
        serial.cpp \
        message/statusMessage.cpp \
        setDialog.cpp \
        settingsDialog.cpp \
        utils.cpp

HEADERS += \
        commandlineparser.h \
        debugIntegerDialog.h \
        aboutDialog.h \
        chartWidget.h \
        debugDialog.h \
        fileLogger.h \
        mainWindow.h \
        message/additionalSettingsMessage.h \
        message/alertMessage.h \
        message/baseMessage.h \
        message/debugMessage.h \
        message/errorMessage.h \
        messageHandler.h \
        portSelectionDialog.h \
        runtimeconfig.h \
        serialOverseer.h \
        sendMessageDialog.h \
        serial.h \
        message/statusMessage.h \
        setDialog.h \
        settingsDialog.h \
        utils.h

FORMS += \
        debugIntegerDialog.ui \
        aboutDialog.ui \
        chartWidget.ui \
        debugDialog.ui \
        mainWindow.ui \
        portSelectionDialog.ui \
        sendMessageDialog.ui \
        setDialog.ui \
        settingsDialog.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    ../resource.qrc

unix {
    isEmpty(PREFIX) {
        PREFIX = /usr/local
    }

    target.path = $$PREFIX/bin

    shortcutfiles.files = ../util/VentilAid.desktop
    shortcutfiles.path = $$PREFIX/share/applications/

    data.files += ../resource/icon_256x256.png
    data.path = $$PREFIX/

    INSTALLS += shortcutfiles
    INSTALLS += data
}

DISTFILES += \
    ../util/VentilAid.desktop

OTHER_FILES += \
    ../.gitignore \
    ../.clang-format \
    ../.gitlab-ci.yml

#include "runtimeconfig.h"
#include <QList>
#include <QtDebug>
#include <QFileInfo>

QList<uint> availableBaudRateValues = {115200, 57600, 38400, 19200, 9600,4800, 2400, 1200};

RuntimeConfig* RuntimeConfig::runtimeConfiguration = nullptr;

RuntimeConfig::~RuntimeConfig() {
    if(runtimeConfiguration != nullptr) {
        delete runtimeConfiguration;
        runtimeConfiguration = nullptr;
    }
}

RuntimeConfig*  RuntimeConfig::getRuntimeConfig(){return runtimeConfiguration;};

void RuntimeConfig::createRuntimeConfig(uint baudRate, QString serialPort, bool skipCrc /*=false*/) {
    if(runtimeConfiguration == nullptr) {
        runtimeConfiguration= new RuntimeConfig(baudRate,serialPort,skipCrc);
        return;
    }
    qWarning()<<"Runtime configuration already exists!";
}

RuntimeConfig::RuntimeConfig(uint baudRate, QString serialPort, bool skipCrc /*=false*/ )
{
  evaluateBaudRate(baudRate);
  evaluateSerialPort(serialPort);

  configuration.baudRate = baudRate;
  configuration.serialPort = serialPort;
  configuration.skipCrc = skipCrc;
}

void RuntimeConfig::evaluateBaudRate(uint baudRate)
{
    for(auto it = availableBaudRateValues.begin(); it!=availableBaudRateValues.end(); it++){
        if(*it== baudRate) {
            isConfigValid = true;
            return;
        }
    }
    qWarning() << "Baud rate has the incorrect value. The program will now exit";
    isConfigValid = false;
}

void RuntimeConfig::evaluateSerialPort(QString serialPort)
{
    QString path ="/dev/"+ serialPort;
    QFileInfo fileInfo(path);

    if(fileInfo.exists())
    {
        isConfigValid = true;
        return;
    }

    qWarning() <<  "Serial port has the incorrect value. The program will now exit";
    isConfigValid = false;
}

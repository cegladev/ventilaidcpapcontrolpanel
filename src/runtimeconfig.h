#ifndef RUNTIMECONFIG_H
#define RUNTIMECONFIG_H
#include <qstring.h>

class RuntimeConfig
{
    struct RuntimeConfigValue {
        uint    baudRate;
        QString serialPort;
        bool    skipCrc;
    };

public:

     static void            createRuntimeConfig(uint baudRate, QString serialPort, bool skipCrc /*=false*/);
     static RuntimeConfig*  getRuntimeConfig();

     RuntimeConfigValue     getConfiguration() { return configuration; };
     bool                   isConfigurationValid(){return isConfigValid;};

private:
    RuntimeConfig(uint baudRate, QString serialPort, bool skipCrc= false );
    RuntimeConfig();
    ~RuntimeConfig();

    void    evaluateBaudRate(uint baudRate);
    void    evaluateSerialPort(QString serialPort);


    RuntimeConfigValue      configuration;
    static RuntimeConfig*    runtimeConfiguration;
    bool                    isConfigValid= false;
};

#endif // RUNTIMECONFIG_H

#include "errorMessage.h"

#include <QByteArray>

ErrorMessage::ErrorMessage(const QByteArray& data)
{
    const char* ptr = data.data();

    errorId = *((uint8_t*)(ptr));

    // TODO: check crc
}

uint8_t ErrorMessage::getErrorId() const
{
    return errorId;
}

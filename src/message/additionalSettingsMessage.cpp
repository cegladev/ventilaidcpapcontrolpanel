#include "additionalSettingsMessage.h"

AdditionalSettingsMessage::AdditionalSettingsMessage(float P, float I, float D, float max,
    float min, float deltaP, float deltaT,
    float deltaF)
    : P(P)
    , I(I)
    , D(D)
    , max(max)
    , min(min)
    , deltaP(deltaP)
    , deltaT(deltaT)
    , deltaF(deltaF)
{
}

QByteArray AdditionalSettingsMessage::getData() const
{
    QByteArray data(SIZE, 0);
    char* ptr = data.data();

    // header
    *((uint8_t*)(ptr)) = START_MARKER_ARRAY[0];
    *((uint8_t*)(ptr + 1)) = START_MARKER_ARRAY[1];
    *((uint8_t*)(ptr + 2)) = MSG_ID;
    *((uint8_t*)(ptr + 3)) = SIZE;

    // data
    *((float*)(ptr + 4)) = P;
    *((float*)(ptr + 8)) = I;
    *((float*)(ptr + 12)) = D;
    *((float*)(ptr + 16)) = max;
    *((float*)(ptr + 20)) = min;
    *((float*)(ptr + 24)) = deltaP;
    *((float*)(ptr + 28)) = deltaT;
    *((float*)(ptr + 32)) = deltaF;

    // reserved for future use
    ptr[36] = ptr[37] = ptr[38] = ptr[39] = ptr[40] = 0;

    // footer
    ptr[41] = ptr[42] = 0; // TODO: Add CRC
    *((uint8_t*)(ptr + 43)) = END_BYTE;

    return data;
}

#include "debugMessage.h"

#include <QByteArray>

DebugMessage::DebugMessage(const QByteArray& data)
{
    const char* ptr = data.data();

    debugValue[0] = *((float*)(ptr + 4));
    debugValue[1] = *((float*)(ptr + 8));
    debugValue[2] = *((float*)(ptr + 12));
    debugValue[3] = *((float*)(ptr + 16));

    intergerValue = *((uint8_t*)(ptr + 20));
}

float DebugMessage::getValue(int idx) const
{
    return debugValue[idx];
}

uint8_t DebugMessage::getIntergerValue() const
{
    return intergerValue;
}

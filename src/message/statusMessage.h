#ifndef STATUSMESSAGE_H
#define STATUSMESSAGE_H

#include <cstddef>

#include "baseMessage.h"

#include <QByteArray>

class StatusMessage : public BaseMessage {
public:
    const static uint8_t SIZE = 32;
    const static uint8_t SEND_MSG_ID = 100;
    const static uint8_t RECEIVE_MSG_ID = 1;

    StatusMessage(float highPressure, float lowPressure, float airVolume, float breathsPerMinute, float breathProportion);
    StatusMessage(const QByteArray& data);

    QByteArray getData() const;

    float getHighPressure() const;
    float getLowPressure() const;
    float getAirVolume() const;
    float getBreathsPerMinute() const;
    float getBreathProportion() const;

private:
    float highPressure;
    float lowPressure;
    float airVolume;
    float breathsPerMinute;
    float breathProportion;
};

#endif // MESSAGE_H

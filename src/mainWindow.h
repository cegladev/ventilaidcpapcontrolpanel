#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "message/statusMessage.h"
#include "utils.h"

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class QProgressDialog;
class DebugDialog;
class DebugIntegerDialog;
class SerialOverseer;

class MainWindow : public QMainWindow {
    Q_OBJECT

public:
    static constexpr float SLIDER_FACTOR = 10.f;

    explicit MainWindow(QWidget* parent, Utils::Target target, SerialOverseer* serial);
    ~MainWindow();

private slots:
    void onConnectionStausChanged(bool connected, QString portName);
    void onNewMessage(StatusMessage msg);
    void sendMessage();
    void openUserSelectedSerial();
    void openPreviousSerial();

private:
    void setupMenu();
    void setupSliders();
    void setupCharts();

    Ui::MainWindow* ui;

    const Utils::Target target;
    QProgressDialog* progressDialog;
    DebugDialog* debugDialog;
    DebugIntegerDialog* debugIntegerDialog;
    SerialOverseer* serialOverseer;
};

#endif // MAINWINDOW_H

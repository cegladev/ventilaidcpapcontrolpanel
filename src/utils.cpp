#include "utils.h"

#include <iostream>

#include <QCoreApplication>
#include <QFileInfo>
#include <QSettings>

void Utils::initSettingsWithDefult()
{
    QCoreApplication::setOrganizationName("VentilAid");
    QCoreApplication::setOrganizationDomain("ventilaid.org");
    QCoreApplication::setApplicationName("VentilAid CPAP Control Panel");

    const QMap<QString, int> DEFAULT_VALUES_INT = {
        { "serialTimeoutMs", 5000 },
        { "sendIntervalMs", 1000 },
        { "refreshRateHz", 10 },
        { "chartWindowSeconds", 30 },
        { "lastSelectedBaudRate", 115200 },
    };

    const QMap<QString, QString> DEFAULT_VALUES_STRING = {
        { "lastSelectedPort", "" },
        { "loggingDirPath", "" },
    };

    const QMap<QString, bool> DEFAULT_VALUES_BOOL = {
        { "autoReconnect", true },
        { "gatherLogs", true },
    };

    const QMap<QString, int> HIGH_PRESSURE_DEFAULT = {
        { "chartWindowSeconds", 30 },
        { "min", 5 },
        { "default", 5 },
        { "max", 15 },
    };

    const QMap<QString, int> LOW_PRESSURE_DEFAULT = {
        { "chartWindowSeconds", 30 },
        { "min", 3 },
        { "default", 5 },
        { "max", 15 },
    };

    const QMap<QString, int> BREATHS_DEFAULT = {
        { "chartWindowSeconds", 30 },
        { "min", 10 },
        { "default", 12 },
        { "max", 40 },
    };

    const QMap<QString, int> BREATH_PROPORTION_DEFAULT = {
        { "chartWindowSeconds", 30 },
        { "min", 2 },
        { "default", 3 },
        { "max", 4 },
    };

    const QMap<QString, int> AIR_VOLUME_DEFAULT = {
        { "chartWindowSeconds", 30 },
        { "min", 300 },
        { "default", 400 },
        { "max", 600 },
    };

    const std::map<QString, QMap<QString, int>> DEFAULT_VALUES_CHARTS = {
        { "highPressure", HIGH_PRESSURE_DEFAULT },
        { "lowPressure", LOW_PRESSURE_DEFAULT },
        { "breaths", BREATHS_DEFAULT },
        { "breathProportion", BREATH_PROPORTION_DEFAULT },
        { "airVolume", AIR_VOLUME_DEFAULT },
    };

    QSettings settings;

    std::cout << "Config path: " << settings.fileName().toStdString() << std::endl;

    {
        for (const auto& chart : DEFAULT_VALUES_CHARTS) {
            settings.beginGroup(chart.first);

            auto existingKeys = settings.childKeys();
            for (auto key : chart.second.keys())
                if (!existingKeys.contains(key))
                    settings.setValue(key, chart.second[key]);

            settings.endGroup();
        }
    }

    {
        auto existingKeys = settings.allKeys();

        for (auto key : DEFAULT_VALUES_INT.keys())
            if (!existingKeys.contains(key))
                settings.setValue(key, DEFAULT_VALUES_INT[key]);

        for (auto key : DEFAULT_VALUES_STRING.keys())
            if (!existingKeys.contains(key))
                settings.setValue(key, DEFAULT_VALUES_STRING[key]);

        for (auto key : DEFAULT_VALUES_BOOL.keys())
            if (!existingKeys.contains(key))
                settings.setValue(key, DEFAULT_VALUES_BOOL[key]);
    }

    {
        const QStringList debugCharts = {
            "debugChart1",
            "debugChart2",
            "debugChart3",
            "debugChart4",
            "debugIntegerChart1"
        };

        const QMap<QString, int> DEFAULT_CHART_VALUES = {
            { "chartWindowSeconds", 30 },
            { "axisYMin", 0 },
            { "axisYMax", 20 },
        };

        for (const auto& chartName : debugCharts) {
            settings.beginGroup(chartName);

            auto existingKeys = settings.childKeys();
            for (auto key : DEFAULT_CHART_VALUES.keys())
                if (!existingKeys.contains(key))
                    settings.setValue(key, DEFAULT_CHART_VALUES[key]);

            settings.endGroup();
        }
    }
}

bool Utils::isPathWritable(QString path)
{
    QFileInfo fileInfo(path);
    return fileInfo.isWritable();
}

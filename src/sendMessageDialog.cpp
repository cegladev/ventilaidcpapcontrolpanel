#include "sendMessageDialog.h"
#include "ui_sendMessageDialog.h"

#include <QSettings>

SendMessageDialog::SendMessageDialog(QWidget* parent)
    : QDialog(parent)
    , ui(new Ui::SendMessageDialog)
{
    ui->setupUi(this);

    const QList<QDoubleSpinBox*> widgets = {
        ui->P,
        ui->I,
        ui->D,
        ui->max,
        ui->min,
        ui->deltaP,
        ui->deltaT,
        ui->deltaF,
    };

    for (auto widget : widgets)
        widget->setRange(std::numeric_limits<double>::lowest(), std::numeric_limits<double>::max());

    QSettings settings;

    settings.beginGroup("PID");

    ui->P->setValue(settings.value("P", 0.0).toFloat());
    ui->I->setValue(settings.value("I", 0.0).toFloat());
    ui->D->setValue(settings.value("D", 0.0).toFloat());
    ui->max->setValue(settings.value("max", 0.0).toFloat());
    ui->min->setValue(settings.value("min", 0.0).toFloat());
    ui->deltaP->setValue(settings.value("deltaP", 0.0).toFloat());
    ui->deltaT->setValue(settings.value("deltaT", 0.0).toFloat());
    ui->deltaF->setValue(settings.value("deltaF", 0.0).toFloat());
}

SendMessageDialog::~SendMessageDialog()
{
    delete ui;
}

AdditionalSettingsMessage SendMessageDialog::getMessage() const
{
    QSettings settings;

    settings.beginGroup("PID");

    settings.setValue("P", ui->P->value());
    settings.setValue("I", ui->I->value());
    settings.setValue("D", ui->D->value());
    settings.setValue("max", ui->max->value());
    settings.setValue("min", ui->min->value());
    settings.setValue("deltaP", ui->deltaP->value());
    settings.setValue("deltaT", ui->deltaT->value());
    settings.setValue("deltaF", ui->deltaF->value());

    return AdditionalSettingsMessage(ui->P->value(), ui->I->value(), ui->D->value(),
        ui->max->value(), ui->min->value(),
        ui->deltaP->value(), ui->deltaT->value(), ui->deltaF->value());
}

#ifndef SETDIALOG_H
#define SETDIALOG_H

#include <QDialog>

namespace Ui {
class SetDialog;
}

class SetDialog : public QDialog {
    Q_OBJECT

public:
    explicit SetDialog(QString name, float value, QString settingsKey, QString unit, float sliderFactor);
    ~SetDialog();

    float getValue() const;

private:
    const float sliderFactor;

    Ui::SetDialog* ui;
};

#endif // SETDIALOG_H

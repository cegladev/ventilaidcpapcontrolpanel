#include "mainWindow.h"
#include "ui_mainWindow.h"

#include "aboutDialog.h"
#include "debugDialog.h"
#include "debugIntegerDialog.h"
#include "portSelectionDialog.h"
#include "sendMessageDialog.h"
#include "serialOverseer.h"
#include "setDialog.h"
#include "settingsDialog.h"
#include "utils.h"

#include <QDebug>
#include <QInputDialog>
#include <QMessageBox>
#include <QProgressDialog>
#include <QtCharts/QLineSeries>

MainWindow::MainWindow(QWidget* parent, Utils::Target target, SerialOverseer* serial)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
    , target(target)
    , progressDialog(nullptr)
    , debugDialog(nullptr)
    , debugIntegerDialog(nullptr)
    , serialOverseer(serial)
{
    ui->setupUi(this);

    connect(serialOverseer, &SerialOverseer::connected, [this](QString portName) { onConnectionStausChanged(true, portName); });

    connect(serialOverseer, &SerialOverseer::disconnected, [this](Serial::DisconnectionReason reason, QString msg) {
        onConnectionStausChanged(false, "");

        // Report errors
        if (reason == Serial::DisconnectionReason::Error) {
            statusBar()->showMessage(msg);

            if (serialOverseer->isReconnecting())
                this->progressDialog->setLabelText(QString("Connect failed with: %1").arg(msg));
        }
    });

    connect(serialOverseer, &SerialOverseer::reconnectingStarted, [this](QString msg) {
        progressDialog = new QProgressDialog(QString("Connect failed with: %1").arg(msg), "Cancel", 0, 0, this);
        progressDialog->setWindowModality(Qt::WindowModal);
        progressDialog->setWindowTitle("Reconnecting");

        if (this->target == Utils::Target::Raspberry)
            progressDialog->setCancelButton(nullptr);

        connect(serialOverseer, &SerialOverseer::connected, progressDialog, [this]() {
            progressDialog->cancel();
            serialOverseer->stopReconnecting();
            progressDialog->deleteLater();
            progressDialog = nullptr;
        });
        connect(progressDialog, &QProgressDialog::canceled, [this]() {
            qDebug() << "Reconnection canceled";

            serialOverseer->stopReconnecting();
            progressDialog->deleteLater();
            progressDialog = nullptr;

            if (this->target == Utils::Target::Raspberry)
                this->openPreviousSerial();
        });

        progressDialog->show();
    });

    connect(serialOverseer, &SerialOverseer::newStatusMessage, this, &MainWindow::onNewMessage);

    if (this->target == Utils::Target::Raspberry) {
        statusBar()->hide();
        menuBar()->hide();
        QApplication::setOverrideCursor(Qt::BlankCursor);
        showFullScreen();
    } else {
        debugDialog = new DebugDialog(this);
        debugIntegerDialog = new DebugIntegerDialog(this);
        connect(serialOverseer, &SerialOverseer::newDebugMessage, debugDialog, &DebugDialog::onNewDebugMessage);
        connect(serialOverseer, &SerialOverseer::newDebugMessage, debugIntegerDialog, &DebugIntegerDialog::onNewDebugMessage);
    }

    connect(serialOverseer, &SerialOverseer::statusMessageRequest, this, &MainWindow::sendMessage);

    setupMenu();
    setupSliders();
    setupCharts();

    onConnectionStausChanged(false, "");

    // without this mainwindow layout breaks on Raspberry Pi
    adjustSize();

    if (this->target == Utils::Target::Raspberry)
        QTimer::singleShot(0, [this]() {
            openPreviousSerial();
        });
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::onConnectionStausChanged(bool connected, QString portName)
{
    const QList<QWidget*> widgets = {
        ui->setHighPressure,
        ui->setLowPressure,
        ui->setBreaths,
        ui->setBreathProportion,
        ui->setAirVolume
    };

    for (auto widget : widgets)
        widget->setEnabled(connected);

    const QList<QWidget*> sliders = {
        ui->highPressureSlider,
        ui->lowPressureSlider,
        ui->airVolumeSlider,
        ui->breathsSlider,
        ui->breathProportionSlider,
    };

    for (auto widget : sliders)
        if (target == Utils::Target::Raspberry)
            widget->setEnabled(false);
        else
            widget->setEnabled(connected);

    ui->actionSendMessage->setEnabled(connected);
    ui->actionSettings->setEnabled(!connected);
    ui->actionShowDebug->setEnabled(connected);

    if (connected) {
        ui->actionConnection->setText("Disconnect");
        statusBar()->showMessage("Connected to: " + portName);

        if (this->target == Utils::Target::Desktop) {
            debugDialog->show();
            debugIntegerDialog->show();
        }
    } else {
        ui->actionConnection->setText("Connect");
        statusBar()->showMessage("Not connected");
    }

    if (!connected) {
        ui->highPressureChart->clear();
        ui->lowPressureChart->clear();
        ui->breathsChart->clear();
        ui->breathProportionChart->clear();
        ui->airVolumeChart->clear();

        if (this->target == Utils::Target::Desktop) {
            debugDialog->clear();
            debugIntegerDialog->clear();
        }
    }
}

void MainWindow::onNewMessage(StatusMessage msg)
{
    ui->highPressureChart->addValue(msg.getHighPressure());
    ui->lowPressureChart->addValue(msg.getLowPressure());
    ui->breathsChart->addValue(msg.getBreathsPerMinute());
    ui->breathProportionChart->addValue(msg.getBreathProportion());
    ui->airVolumeChart->addValue(msg.getAirVolume());
}

void MainWindow::sendMessage()
{
    StatusMessage msg(
        ui->highPressureSlider->value() / SLIDER_FACTOR,
        ui->lowPressureSlider->value() / SLIDER_FACTOR,
        ui->airVolumeSlider->value() / SLIDER_FACTOR,
        ui->breathsSlider->value() / SLIDER_FACTOR,
        ui->breathProportionSlider->value() / SLIDER_FACTOR);
    serialOverseer->sendMessage(msg);
}

void MainWindow::setupMenu()
{
    connect(ui->actionConnection, &QAction::triggered, [this]() {
        if (this->serialOverseer->isConnected())
            this->serialOverseer->disconnect();
        else {
            bool isLogDirValid = Utils::isPathWritable(QSettings().value("loggingDirPath").toString());
            if (isLogDirValid)
                this->openUserSelectedSerial();
            else {
                QMessageBox msgBox(QMessageBox::Icon::Warning, "Connection error", "Please set correct logging directory before connecting");
                msgBox.exec();

                SettingsDialog dialog;
                dialog.exec();
            }
        }
    });

    connect(ui->actionSendMessage, &QAction::triggered, [this]() {
        SendMessageDialog dialog;
        auto result = dialog.exec();
        if (result == QDialog::Accepted && serialOverseer->isConnected())
            serialOverseer->sendMessage(dialog.getMessage());
    });

    connect(ui->actionSettings, &QAction::triggered, []() {
        SettingsDialog dialog;
        dialog.exec();
    });

    connect(ui->actionAbout, &QAction::triggered, []() {
        AboutDialog dialog;
        dialog.exec();
    });

    if (this->target == Utils::Target::Desktop) {
        connect(ui->actionShowDebug, &QAction::triggered, [this]() {
            debugDialog->show();
            debugIntegerDialog->show();
        });
    }
}

void MainWindow::setupSliders()
{
    const QVector<std::tuple<QPushButton*, QString, QSlider*, QString, QString>> connectParams = {
        std::make_tuple(ui->setHighPressure, "Higher pressure", ui->highPressureSlider, "highPressure", "%1 cmH20"),
        std::make_tuple(ui->setLowPressure, "Lower pressure", ui->lowPressureSlider, "lowPressure", "%1 cmH20"),
        std::make_tuple(ui->setBreaths, "Breaths", ui->breathsSlider, "breaths", "%1/min"),
        std::make_tuple(ui->setBreathProportion, "Breath proportion", ui->breathProportionSlider, "breathProportion", "1/%1"),
        std::make_tuple(ui->setAirVolume, "Air volume", ui->airVolumeSlider, "airVolume", "%1 mL")
    };

    // Packaging as AppImage requires Ubuntu 16.04. Since 16.04 has old gcc compiler we cannot for use modern tuple unpakcing :(
    //  for (auto [button, label, slider, minKey, maxKey] : connectParams) {
    for (auto tuple : connectParams) {
        QPushButton* button;
        QString label, settingsKey, unitTemplate;
        QSlider* slider;
        std::tie(button, label, slider, settingsKey, unitTemplate) = tuple;

        connect(button, &QPushButton::clicked, [this, label, slider, settingsKey, unitTemplate]() {
            if (target == Utils::Target::Raspberry) {
                SetDialog dialog(label, slider->value() / SLIDER_FACTOR, settingsKey, unitTemplate, SLIDER_FACTOR);
                if (this->target == Utils::Target::Raspberry)
                    dialog.showMaximized();

                auto dialogStatus = dialog.exec();
                if (dialogStatus == QDialog::Accepted)
                    slider->setValue(dialog.getValue() * SLIDER_FACTOR);
            } else {
                QSettings settings;
                settings.beginGroup(settingsKey);

                bool ok;
                double i = QInputDialog::getDouble(this, "Set", label,
                    slider->value() / SLIDER_FACTOR,
                    settings.value("min").toFloat(),
                    settings.value("max").toFloat(),
                    1, &ok);
                if (ok)
                    slider->setValue(i * SLIDER_FACTOR);
            }
        });
    }

    const QVector<std::tuple<QLabel*, QString, QSlider*, QString>> SLIDER_PARAMS = {
        std::make_tuple(ui->highPressureSetLabel, "highPressure", ui->highPressureSlider, "%1 cmH20"),
        std::make_tuple(ui->lowPressureSetLabel, "lowPressure", ui->lowPressureSlider, "%1 cmH20"),
        std::make_tuple(ui->breathsSetLabel, "breaths", ui->breathsSlider, "%1/min"),
        std::make_tuple(ui->breathProportionSetLabel, "breathProportion", ui->breathProportionSlider, "1/%1"),
        std::make_tuple(ui->airVolumeSetLabel, "airVolume", ui->airVolumeSlider, "%1 mL")
    };

    for (auto tuple : SLIDER_PARAMS) {
        QLabel* label;
        QString settingsKey, unitTemplate;
        QSlider* slider;
        std::tie(label, settingsKey, slider, unitTemplate) = tuple;

        QSettings settings;
        settings.beginGroup(settingsKey);

        slider->setRange(settings.value("min").toFloat() * SLIDER_FACTOR, settings.value("max").toFloat() * SLIDER_FACTOR);
        slider->setValue(settings.value("default").toFloat() * SLIDER_FACTOR);
        label->setText(unitTemplate.arg(settings.value("default").toFloat(), 4, 'f', 1));

        connect(slider, &QSlider::valueChanged, [label, unitTemplate](int value) {
            label->setText(unitTemplate.arg(value / SLIDER_FACTOR, 4, 'f', 1));
        });
    }
}

void MainWindow::setupCharts()
{
    const QVector<std::tuple<ChartWidget*, QString, QString>> CHARTS = {
        std::make_tuple(ui->highPressureChart, "highPressure", "%1 cmH20"),
        std::make_tuple(ui->lowPressureChart, "lowPressure", "%1 cmH20"),
        std::make_tuple(ui->breathsChart, "breaths", "%1/min"),
        std::make_tuple(ui->breathProportionChart, "breathProportion", "1/%1"),
        std::make_tuple(ui->airVolumeChart, "airVolume", "%1 mL"),
    };

    for (const auto tuple : CHARTS) {
        ChartWidget* chart;
        QString settingsKey, unitTemplate;
        std::tie(chart, settingsKey, unitTemplate) = tuple;

        chart->setup(settingsKey, unitTemplate);
    }
}

void MainWindow::openUserSelectedSerial()
{
    if (QSerialPortInfo::availablePorts().empty()) {
        QMessageBox msgBox(QMessageBox::Icon::Critical, "Error", "No serial ports are available");
        msgBox.exec();
        return;
    }

    PortSelectionDialog dialog;
    auto dialogStatus = dialog.exec();
    if (dialogStatus == QDialog::Rejected)
        return;

    serialOverseer->open(dialog.getSeriaPortName(), dialog.getBaudRate());
}

void MainWindow::openPreviousSerial()
{
    QSettings settings;

    serialOverseer->open(
        settings.value("lastSelectedPort").toString(),
        static_cast<QSerialPort::BaudRate>(settings.value("lastSelectedBaudRate").toInt()));
}

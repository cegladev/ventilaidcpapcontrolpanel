#ifndef SERIAL_H
#define SERIAL_H

#include <QObject>
#include <QtSerialPort>

#include "message/additionalSettingsMessage.h"
#include "message/alertMessage.h"
#include "message/debugMessage.h"
#include "message/errorMessage.h"
#include "message/statusMessage.h"

class Serial : public QObject {
    Q_OBJECT

public:
    enum class DisconnectionReason {
        UserRequest,
        Error
    };

    explicit Serial(QObject* parent);

    bool isConnected() const;

    void disconnect();

signals:
    void connected(QString port);
    void disconnected(DisconnectionReason reason, QString message);
    void newStatusMessage(StatusMessage msg);
    void newDebugMessage(DebugMessage msg);
    void newErrorMessage(ErrorMessage msg);
    void newAlertMessage(AlertMessage msg);

public slots:
    void sendMessage(StatusMessage msg);
    void sendMessage(AdditionalSettingsMessage msg);
    void open(QString portName, QSerialPort::BaudRate baudRate);

private slots:
    void close(DisconnectionReason reason, QString messsage);
    void handleReadyRead();
    void handleTimeout();
    void handleError(QSerialPort::SerialPortError error);

private:
    bool parseMessage();

private:
    const size_t TIMEOUT_MS;

    QSerialPort* serialPort;
    QTimer* watchdog;
    bool _connected;
    QByteArray dataBuffer;
};

#endif // SERIAL_H

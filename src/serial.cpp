#include "serial.h"

#include <QSettings>

Serial::Serial(QObject* parent)
    : QObject(parent)
    , TIMEOUT_MS(QSettings().value("serialTimeoutMs").toUInt())
    , serialPort(nullptr)
    , watchdog(new QTimer(this))
    , _connected(false)
{
    connect(watchdog, &QTimer::timeout, this, &Serial::handleTimeout);
}

bool Serial::isConnected() const
{
    return this->_connected;
}

void Serial::disconnect()
{
    close(DisconnectionReason::UserRequest, "");
}

void Serial::open(QString portName, QSerialPort::BaudRate baudRate)
{
    serialPort = new QSerialPort(portName, this);
    serialPort->setBaudRate(baudRate);

    if (!serialPort->open(QIODevice::ReadWrite)) {
        auto errorMsg = QString("Failed to open port: '%1' With error: %2").arg(portName, serialPort->errorString());
        delete serialPort;
        serialPort = nullptr;

        emit disconnected(DisconnectionReason::Error, errorMsg);
    } else {
        connect(serialPort, &QSerialPort::readyRead, this, &Serial::handleReadyRead);
        connect(serialPort, &QSerialPort::errorOccurred, this, &Serial::handleError);

        _connected = true;
        watchdog->start(TIMEOUT_MS);
        emit connected(serialPort->portName());
    }
}

void Serial::close(DisconnectionReason reason, QString message)
{
    watchdog->stop();
    // using `delete` instead of `deleteLater()` was causing crashes when manually disconecting serial cable
    serialPort->deleteLater();
    serialPort = nullptr;
    dataBuffer.resize(0);
    _connected = false;

    emit disconnected(reason, message);
}

void Serial::sendMessage(StatusMessage msg)
{
    if (serialPort == nullptr) {
        qDebug() << "Error: Calling send message on closed serial";
        return;
    }

    auto data = msg.getData();
    const qint64 bytesWrtitten = serialPort->write(data);

    if (bytesWrtitten != data.size()) {
        auto errorMsg = QString("Serial write failed with %1. Closing connection.").arg(serialPort->errorString());
        close(DisconnectionReason::Error, errorMsg);
    }
}

void Serial::sendMessage(AdditionalSettingsMessage msg)
{
    if (serialPort == nullptr) {
        qDebug() << "Error: Calling send message on closed serial";
        return;
    }

    auto data = msg.getData();
    const qint64 bytesWrtitten = serialPort->write(data);

    if (bytesWrtitten != data.size()) {
        auto errorMsg = QString("Serial write failed with %1. Closing connection.").arg(serialPort->errorString());
        close(DisconnectionReason::Error, errorMsg);
    }
}

void Serial::handleReadyRead()
{
    if (serialPort == nullptr) {
        qDebug() << "Error: Calling handle read on closed serial";
        return;
    }

    watchdog->start(TIMEOUT_MS);

    dataBuffer.append(serialPort->readAll());

    while (parseMessage())
        ;
}

bool Serial::parseMessage()
{
    auto idx = dataBuffer.indexOf(BaseMessage::START_MARKER_ARRAY);
    if (idx == -1) {
        // It is possible that part of the start marker is already at the end of dataBuffer
        // so we can't clear the entire dataBuffer.
        dataBuffer.remove(0, dataBuffer.size() - BaseMessage::START_MARKER_ARRAY.size());
        return false;
    } else
        dataBuffer.remove(0, idx);

    if (dataBuffer.size() < BaseMessage::HEADER_SIZE)
        return false; // we have to wait for more data in the buffer

    uint8_t* ptr = (uint8_t*)(dataBuffer.data());
    uint8_t msgId = ptr[2];
    uint8_t msgSize = ptr[3];

    if (dataBuffer.size() < msgSize)
        return false; // we have to wait for more data in the buffer

    uint8_t lastByte = *(ptr + msgSize - 1);
    if (lastByte != BaseMessage::END_BYTE) {
        qDebug() << "Discarding one byte, because of incorrect end marker: " << lastByte;

        // We know that dataBuffer starts with startMarker, so we remove one byte and try again
        dataBuffer.remove(0, 1);
        return true;
    }

    if (msgId == StatusMessage::RECEIVE_MSG_ID) {
        StatusMessage msg(dataBuffer.left(msgSize));
        dataBuffer.remove(0, msgSize);

        emit newStatusMessage(msg);
        return true;
    } else if (msgId == ErrorMessage::MSG_ID) {
        ErrorMessage msg(dataBuffer.left(msgSize));
        dataBuffer.remove(0, msgSize);

        emit newErrorMessage(msg);
        return true;
    } else if (msgId == DebugMessage::MSG_ID) {
        DebugMessage msg(dataBuffer.left(msgSize));
        dataBuffer.remove(0, msgSize);

        emit newDebugMessage(msg);
        return true;
    } else if (msgId == AlertMessage::MSG_ID) {
        AlertMessage msg(dataBuffer.left(msgSize));
        dataBuffer.remove(0, msgSize);

        emit newAlertMessage(msg);
        return true;
    } else {
        qDebug() << "Discarding one byte, because of incorrect msgId: " << msgId;

        // We know that dataBuffer starts with startMarker, so we remove one byte and try again
        dataBuffer.remove(0, 1);
        return true;
    }
}

void Serial::handleTimeout()
{
    auto errorMsg = QString("No data was recieved for %1ms. Closing connection.").arg(TIMEOUT_MS);
    close(DisconnectionReason::Error, errorMsg);
}

void Serial::handleError(QSerialPort::SerialPortError serialPortError)
{
    if (serialPortError == QSerialPort::NoError)
        return;

    auto errorMsg = QString("Serial port failed (error id=%1) with %2. Closing connection.").arg(QString::number(serialPortError), serialPort->errorString());
    close(DisconnectionReason::Error, errorMsg);
}

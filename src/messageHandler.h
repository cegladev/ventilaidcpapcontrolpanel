#ifndef MESSAGEHANDLER_H
#define MESSAGEHANDLER_H

#include <QDate>
#include <QObject>

#include "message/alertMessage.h"
#include "message/debugMessage.h"
#include "message/errorMessage.h"
#include "message/statusMessage.h"

class FileLogger;

class MessageHandler : public QObject {
    Q_OBJECT
public:
    explicit MessageHandler(QObject* parent);

public slots:
    void newStatusMessage(StatusMessage msg);
    void newDebugMessage(DebugMessage msg);
    void newAlertMessage(AlertMessage msg);
    void newErrorMessage(ErrorMessage msg);

private:
    void checkLoggerStatus();
    void resetLoggers();

private:
    QDate currentDate;
    FileLogger* statusLogger;
    FileLogger* debugLogger;
    FileLogger* alertLogger;
};

#endif // MESSAGEHANDLER_H

#include "commandlineparser.h"
#include "runtimeconfig.h"
#include <QDebug>

CommandLineParser::CommandLineParser():
    baudRate( "baudRate",
              QCoreApplication::translate("main", "Sets value of baud rate"),
              QCoreApplication::translate("main", "baudRate")),
    serialPort( "serialPort",
                QCoreApplication::translate("main", "Sets the serial Port"),
                QCoreApplication::translate("main", "serialPort_value")),
    skipCrc( "skipCrc",
             QCoreApplication::translate("main", "Sets if you want to skip calculating the CRC"),
             QCoreApplication::translate("main", "skipCrc"))
{
  addVersionOption();
  addHelpOption();
  addOptions({baudRate,serialPort,skipCrc});
}

int CommandLineParser::getReturnCode()
{
    return returnCode;
}

bool CommandLineParser::areInputParamsCorrect()
{
    if(isSet(baudRate) && isSet(serialPort)) {
        bool ok = true;

        uint uiBaudRate = value(baudRate).toUInt(&ok);
        if(!ok) {
            qWarning()<<"Baud rate has to a positive intiger";
            returnCode = -1;
            return false;
        }
        QString skipCrcValue = value(skipCrc);

        RuntimeConfig::createRuntimeConfig(uiBaudRate, value(serialPort), skipCrcValue == "true" );
        return RuntimeConfig::getRuntimeConfig()->isConfigurationValid();
    }

    if(isSet(baudRate) || isSet(serialPort)) {
        qWarning()<< "Baud rate and serial port both have to be defined";
        returnCode = -1;
        return false;
    }

    return true;
}

#include "messageHandler.h"

#include "fileLogger.h"

#include <QDateTime>
#include <QDebug>
#include <QDir>
#include <QSettings>

MessageHandler::MessageHandler(QObject* parent)
    : QObject(parent)
    , statusLogger(nullptr)
    , debugLogger(nullptr)
    , alertLogger(nullptr)
{
    resetLoggers();
}

void MessageHandler::checkLoggerStatus()
{
    bool shouldRestartLoggers = currentDate < QDate::currentDate();
    if (shouldRestartLoggers) {
        qDebug() << "Resettting loggers";
        resetLoggers();
    }
}

void MessageHandler::resetLoggers()
{
    currentDate = QDate::currentDate();
    auto dir = QDir(QSettings().value("loggingDirPath").toString());

    delete statusLogger;
    auto statusLogerPath = dir.filePath(currentDate.toString("yyyy.MM.dd") + "_status.csv");
    QString statusHeader("Timestamp;High pressure;Low pressure;Breaths per minute;Breath proportion;Air volume\n");
    statusLogger = new FileLogger(this, statusLogerPath, statusHeader);

    delete debugLogger;
    QString debugHeader("Timestamp;Debug 1;Debug 2;Debug 3;Debug 4;Debug 5\n");
    auto debugLogerPath = dir.filePath(currentDate.toString("yyyy.MM.dd") + "_debug.csv");
    debugLogger = new FileLogger(this, debugLogerPath, debugHeader);

    delete alertLogger;
    QString alertHeader("Timestamp;Alarm Code;Detail1\n");
    auto alertLogerPath = dir.filePath(currentDate.toString("yyyy.MM.dd") + "_alert.csv");
    alertLogger = new FileLogger(this, alertLogerPath, alertHeader);
}

void MessageHandler::newStatusMessage(StatusMessage msg)
{
    checkLoggerStatus();

    QString text = QDateTime::currentDateTime().toString("yyyy.MM.dd hh:mm:ss.zzz");
    text += QString(";%1;%2;%3;%4;%5\n")
                .arg(msg.getHighPressure())
                .arg(msg.getLowPressure())
                .arg(msg.getBreathsPerMinute())
                .arg(msg.getBreathProportion())
                .arg(msg.getAirVolume());

    statusLogger->onNewMessage(text);
}

void MessageHandler::newDebugMessage(DebugMessage msg)
{
    checkLoggerStatus();

    QString text = QDateTime::currentDateTime().toString("yyyy.MM.dd hh:mm:ss.zzz");
    text += QString(";%1;%2;%3;%4;%5\n")
                .arg(msg.getValue(0))
                .arg(msg.getValue(1))
                .arg(msg.getValue(2))
                .arg(msg.getValue(4))
                .arg(msg.getIntergerValue());

    debugLogger->onNewMessage(text);
}

void MessageHandler::newAlertMessage(AlertMessage msg)
{
    checkLoggerStatus();

    QString text = QDateTime::currentDateTime().toString("yyyy.MM.dd hh:mm:ss.zzz");
    text += QString(";%1;%2\n")
                .arg(to_string(msg.getAlarmCode()))
                .arg(msg.getDetail1());

    alertLogger->onNewMessage(text);
}

void MessageHandler::newErrorMessage(ErrorMessage msg)
{
    Q_UNUSED(msg);
    qDebug() << "newErrorMessage is NOT implemented";
}

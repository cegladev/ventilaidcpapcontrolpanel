# VentilAid CPAP Control Panel

This is control panel for [VentilAid](https://www.ventilaid.org/) CPAP device. It implements communication protocol defined [here](https://drive.google.com/file/d/1IZ-owo7fDrHt-_xx8rP0xzdJj9mLZ40c/view).

### Usage

We provide [AppImage](https://appimage.org/) for Linux platform. Simply go to [latest pipeline for master branch](https://gitlab.com/Urbicum/ventilaidcpapcontrolpanel/pipelines/latest), select `Jobs` tab and download artifact for `build` job.
For now for Windows and Mac platforms application has to be built manually.

### Emulating VentilAid CPAP device

For testing UI without VentilAid CPAP device there are two options:
* if you own an Arduino you can use [util/serial_protocol_emulator/serial_protocol_emulator.ino](util/serial_protocol_emulator/serial_protocol_emulator.ino)
* [fakeDataSender.sh](fakeDataSender.sh)  script
